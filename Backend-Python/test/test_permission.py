from fastapi import status


def test_read_document_item(testclient):
    mem = {"username": "newuser"}
    mem_resp = testclient.post("/member/create", json=mem, headers={'Authorization': 'Basic YWRtaW46'})

    aut = {"name": "newauthor"}
    aut_resp = testclient.post("/author/create", json=aut, headers={"Authorization": "Basic aGFtZWQ6"})

    cat = {"name": "newcategory"}
    cat_resp = testclient.post("/category/create", json=cat, headers={"Authorization": "Basic aGFtZWQ6"})

    doc = {"name": "newdoc", "subject": "s", "content": "c",
           "author_id": aut_resp.json()["id"], "category_id": cat_resp.json()["id"]}
    doc_resp = testclient.post("/document/create", json=doc, headers={"Authorization": "Basic aGFtZWQ6"})

    data = {"document_id": doc_resp.json()["id"],
            "member_id": mem_resp.json()["id"],
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    cresp = testclient.post("/permission/create", json=data, headers={"Authorization": "Basic YWRtaW46"})

    resp = testclient.get(f"/permission/document/{cresp.json()['id']}", headers={"Authorization": "Basic aGFtZWQ6"})
    assert resp.status_code == status.HTTP_200_OK, resp.text
    assert resp.json()["id"] == cresp.json()['id']


def test_read_category_item(testclient):
    mem = {"username": "newuser"}
    mem_resp = testclient.post("/member/create", json=mem, headers={'Authorization': 'Basic YWRtaW46'})

    aut = {"name": "newauthor"}
    aut_resp = testclient.post("/author/create", json=aut, headers={"Authorization": "Basic aGFtZWQ6"})

    cat = {"name": "newcategory"}
    cat_resp = testclient.post("/category/create", json=cat, headers={"Authorization": "Basic aGFtZWQ6"})

    data = {"category_id": cat_resp.json()["id"],
            "member_id": mem_resp.json()["id"],
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    cresp = testclient.post("/permission/create", json=data, headers={"Authorization": "Basic YWRtaW46"})

    resp = testclient.get(f"/permission/category/{cresp.json()['id']}", headers={"Authorization": "Basic aGFtZWQ6"})
    assert resp.status_code == status.HTTP_200_OK, resp.text
    assert resp.json()["id"] == cresp.json()['id']


def test_create_item_not_authorized(testclient):
    data = {"document_id": 1,
            "member_id": 3,
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    r = testclient.post("/permission/create", json=data)
    assert r.status_code == status.HTTP_401_UNAUTHORIZED, r.text


def test_create_item_wrong_authorized(testclient):
    data = {"document_id": 1,
            "member_id": 3,
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    r = testclient.post("/permission/create", json=data, headers={"Authorization": "Basic aGFtZWQ6"})
    assert r.status_code == status.HTTP_401_UNAUTHORIZED, r.text


def test_create_document_item(testclient):
    mem = {"username": "newuser"}
    mem_resp = testclient.post("/member/create", json=mem, headers={'Authorization': 'Basic YWRtaW46'})

    aut = {"name": "newauthor"}
    aut_resp = testclient.post("/author/create", json=aut, headers={"Authorization": "Basic aGFtZWQ6"})

    cat = {"name": "newcategory"}
    cat_resp = testclient.post("/category/create", json=cat, headers={"Authorization": "Basic aGFtZWQ6"})

    doc = {"name": "newdoc", "subject": "s", "content": "c",
           "author_id": aut_resp.json()["id"], "category_id": cat_resp.json()["id"]}
    doc_resp = testclient.post("/document/create", json=doc, headers={"Authorization": "Basic aGFtZWQ6"})

    data = {"document_id": doc_resp.json()["id"],
            "member_id": mem_resp.json()["id"],
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    resp = testclient.post("/permission/create", json=data, headers={"Authorization": "Basic YWRtaW46"})
    assert resp.status_code == status.HTTP_201_CREATED, resp.text


def test_create_category_item(testclient):
    mem = {"username": "newuser"}
    mem_resp = testclient.post("/member/create", json=mem, headers={'Authorization': 'Basic YWRtaW46'})

    aut = {"name": "newauthor"}
    aut_resp = testclient.post("/author/create", json=aut, headers={"Authorization": "Basic aGFtZWQ6"})

    cat = {"name": "newcategory"}
    cat_resp = testclient.post("/category/create", json=cat, headers={"Authorization": "Basic aGFtZWQ6"})

    data = {"category_id": cat_resp.json()["id"],
            "member_id": mem_resp.json()["id"],
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    resp = testclient.post("/permission/create", json=data, headers={"Authorization": "Basic YWRtaW46"})
    assert resp.status_code == status.HTTP_201_CREATED, resp.text


def test_create_item_failed(testclient):
    data = {"document_id": 1,
            "category_id": 1,
            "member_id": 3,
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    resp = testclient.post("/permission/create", json=data, headers={"Authorization": "Basic YWRtaW46"})
    assert resp.status_code == status.HTTP_400_BAD_REQUEST, resp.text


def test_create_null_item_failed(testclient):
    data = {"member_id": 3,
            "can_create": False,
            "can_read": True,
            "can_update": True,
            "can_delete": True}
    resp = testclient.post("/permission/create", json=data, headers={"Authorization": "Basic YWRtaW46"})
    assert resp.status_code == status.HTTP_400_BAD_REQUEST, resp.text
