import os

import pytest
from fastapi.testclient import TestClient

os.environ['API_TEST'] = 'True'
from src.main import app


@pytest.fixture(scope="function")
def testclient():

    with TestClient(app) as client:
        data = {"username": "hamed"}
        client.post("/member/create", json=data, headers={'Authorization': 'Basic YWRtaW46'})
        yield client
