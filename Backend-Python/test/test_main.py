from fastapi.testclient import TestClient

from src.main import app


def test_health_endpoint():
    r = TestClient(app).get("/health")
    assert r.status_code == 200
