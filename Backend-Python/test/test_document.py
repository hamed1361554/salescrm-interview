from fastapi import status


def test_read_item(testclient):
    aut = {"name": "newauthor"}
    aut_resp = testclient.post("/author/create", json=aut, headers={"Authorization": "Basic aGFtZWQ6"})

    cat = {"name": "newcategory"}
    cat_resp = testclient.post("/category/create", json=cat, headers={"Authorization": "Basic aGFtZWQ6"})

    data = {"name": "newdoc", "subject": "s", "content": "c",
            "author_id": aut_resp.json()["id"], "category_id": cat_resp.json()["id"]}
    cresp = testclient.post("/document/create", json=data, headers={"Authorization": "Basic aGFtZWQ6"})

    resp = testclient.get(f"/document/{cresp.json()['id']}", headers={"Authorization": "Basic aGFtZWQ6"})
    assert resp.status_code == status.HTTP_200_OK, resp.text
    assert resp.json()["id"] == cresp.json()['id']


def test_create_item_not_authorized(testclient):
    data = {"name": "newdoc", "subject": "s", "content": "c", "author_id": 1, "category_id": 1}
    r = testclient.post("/document/create", json=data)
    assert r.status_code == status.HTTP_401_UNAUTHORIZED, r.text


def test_create_item(testclient):
    aut = {"name": "newauthor"}
    aut_resp = testclient.post("/author/create", json=aut, headers={"Authorization": "Basic aGFtZWQ6"})

    cat = {"name": "newcategory"}
    cat_resp = testclient.post("/category/create", json=cat, headers={"Authorization": "Basic aGFtZWQ6"})

    data = {"name": "newdoc", "subject": "s", "content": "c",
            "author_id": aut_resp.json()["id"], "category_id": cat_resp.json()["id"]}
    resp = testclient.post("/document/create", json=data, headers={"Authorization": "Basic aGFtZWQ6"})
    assert resp.status_code == status.HTTP_201_CREATED, resp.text
    assert resp.json()["name"] == data["name"]


def test_create_item_failed(testclient):
    data = {"name": "newdoc", "subject": "s", "content": "c", "author_id": -1, "category_id": -1}
    resp = testclient.post("/document/create", json=data, headers={"Authorization": "Basic aGFtZWQ6"})
    assert resp.status_code == status.HTTP_404_NOT_FOUND, resp.text
