from fastapi import status


def test_read_item(testclient):
    data = {"username": "newuser"}
    cresp = testclient.post("/member/create", json=data, headers={'Authorization': 'Basic YWRtaW46'})

    resp = testclient.get(f"/member/{cresp.json()['id']}")
    assert resp.status_code == status.HTTP_200_OK, resp.text
    assert resp.json()["id"] == cresp.json()['id']


def test_create_item_not_authorized(testclient):
    data = {"username": "newuser"}
    r = testclient.post("/member/create", json=data)
    assert r.status_code == status.HTTP_401_UNAUTHORIZED, r.text


def test_create_item(testclient):
    data = {"username": "newuser"}
    resp = testclient.post("/member/create", json=data, headers={'Authorization': 'Basic YWRtaW46'})
    assert resp.status_code == status.HTTP_201_CREATED, resp.text
    assert resp.json()["username"] == data["username"]
