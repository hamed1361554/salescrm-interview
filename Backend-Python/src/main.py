import time
import string
import logging
from random import choices

from fastapi import FastAPI
from starlette.requests import Request
from tortoise.contrib.fastapi import register_tortoise
from logging.config import dictConfig

from src.config.logging import log_config
from src.config import tortoise_config, openapi_config
from src.routers import document, permission, member, health, category, author
from src.utils import set_body, get_body

dictConfig(log_config)
logger = logging.getLogger("root")

app = FastAPI(title=openapi_config.name,
              version=openapi_config.version,
              description=openapi_config.description)
app.include_router(member.router, prefix="/member")
app.include_router(document.router, prefix="/document")
app.include_router(permission.router, prefix="/permission")
app.include_router(health.router, prefix="/health")
app.include_router(category.router, prefix="/category")
app.include_router(author.router, prefix="/author")

register_tortoise(
    app,
    db_url=tortoise_config.db_url,
    modules=tortoise_config.modules,
    generate_schemas=tortoise_config.generate_schemas,
    add_exception_handlers=tortoise_config.add_exception_handlers,
)


@app.middleware("http")
async def log_requests(request: Request, call_next):
    idem = ''.join(choices(string.ascii_uppercase + string.digits, k=6))
    # https://stackoverflow.com/questions/69669808/fastapi-custom-middleware-getting-body-of-request-inside
    await set_body(request, await request.body())
    body = await get_body(request)
    logger.info(f"rid={idem} start request path={request.url.path} by verb={request.method} with body={body}")
    start_time = time.time()

    response = await call_next(request)

    process_time = (time.time() - start_time) * 1000
    formatted_process_time = '{0:.2f}'.format(process_time)
    logger.info(f"rid={idem} completed_in={formatted_process_time}ms status_code={response.status_code}")

    return response
