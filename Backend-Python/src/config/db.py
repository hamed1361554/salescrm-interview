from pydantic import Field

from pydantic import BaseSettings
from src.config.cfg import IS_TEST

DB_MODELS = ["src.models.entity.member", "src.models.entity.document", "src.models.entity.permission"]
POSTGRES_DB_URL = "postgres://{postgres_user}:{postgres_password}@{postgres_host}:{postgres_port}/{postgres_db}"
SQLITE_DB_URL = "sqlite://:memory:"


class PostgresSettings(BaseSettings):
    """
    Postgres env values
    """

    postgres_user: str = Field("postgres", env="DB_USER")
    postgres_password: str = Field("postgres", env="DB_PASSWORD")
    postgres_db: str = Field("salescrmdb", env="APP_DB")
    postgres_port: str = Field("5432", env="DB_PORT")
    postgres_host: str = Field("localhost", env="DB_HOST")


class TortoiseSettings(BaseSettings):
    """
    Tortoise settings
    """

    db_url: str
    modules: dict
    generate_schemas: bool
    add_exception_handlers: bool

    @classmethod
    def generate(cls):
        """
        Generate Tortoise-ORM settings (with sqlite if tests)
        """

        if IS_TEST:
            db_url = SQLITE_DB_URL
        else:
            postgres = PostgresSettings()
            db_url = POSTGRES_DB_URL.format(**postgres.dict())

        return TortoiseSettings(db_url=db_url, modules={"models": DB_MODELS},
                                generate_schemas=True, add_exception_handlers=True)
