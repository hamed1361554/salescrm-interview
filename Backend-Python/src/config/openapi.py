from pydantic import BaseSettings

OPENAPI_API_NAME = "Sales CRM API"
OPENAPI_API_VERSION = "0.0.1"
OPENAPI_API_DESCRIPTION = "Sales CRM API"


class OpenAPISettings(BaseSettings):
    name: str
    version: str
    description: str

    @classmethod
    def generate(cls):
        return OpenAPISettings(
            name=OPENAPI_API_NAME,
            version=OPENAPI_API_VERSION,
            description=OPENAPI_API_DESCRIPTION,
        )
