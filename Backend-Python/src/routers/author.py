from fastapi import APIRouter, Depends, status
from tortoise.contrib.pydantic import pydantic_model_creator

from src.auth.basic import check_current_credentials
from src.models.dto.document import AuthorDto
from src.models.entity.document import Author

router = APIRouter()
author_pydantic = pydantic_model_creator(Author)


@router.get("/{id}", status_code=status.HTTP_200_OK)
async def get_author(id: int,
                     username: str = Depends(check_current_credentials)):
    """
    Gets author for given id.
    """

    aut = await Author.get(id=id)
    return await author_pydantic.from_tortoise_orm(aut)


@router.post("/create", status_code=status.HTTP_201_CREATED)
async def create_author(aut: AuthorDto,
                        username: str = Depends(check_current_credentials)):
    """
    Creates author.
    """

    added = await Author.create(name=aut.name)
    return await author_pydantic.from_tortoise_orm(added)
