import logging
from fastapi import APIRouter, Depends, HTTPException, status
from tortoise.contrib.pydantic import pydantic_model_creator

from src.auth.basic import check_current_credentials, check_admin_credentials
from src.models.dto.permission import PermissionDto, PermissionQueryDto
from src.models.entity.document import Category, Document
from src.models.entity.member import Member
from src.models.entity.permission import DocumentPermission, CategoryPermission

router = APIRouter()
logger = logging.getLogger("root")
document_pydantic = pydantic_model_creator(DocumentPermission)
category_pydantic = pydantic_model_creator(CategoryPermission)


@router.get("/validate", status_code=status.HTTP_200_OK)
async def validate_document_permission(query: PermissionQueryDto,
                                       username: str = Depends(check_current_credentials)):
    """
    Gets document permission based on given query.
    """

    member = await Member.get(id=query.member_id)
    doc = await Document.get(id=query.document_id).prefetch_related("category")

    can_create = False
    can_read = False
    can_update = False
    can_delete = False

    cat_permission = await CategoryPermission.get_or_none(category=doc.category,
                                                          member=member)
    if cat_permission is not None:
        can_create = cat_permission.can_create
        can_read = cat_permission.can_read
        can_update = cat_permission.can_create
        can_delete = cat_permission.can_create

    doc_permission = await DocumentPermission.get_or_none(document=doc,
                                                          member=member)
    if doc_permission is not None:
        if doc_permission.can_create:
            can_create = True

        if doc_permission.can_read:
            can_read = True

        if doc_permission.can_update:
            can_update = True

        if doc_permission.can_delete:
            can_delete = True

    return {"can_create": can_create,
            "can_read": can_read,
            "can_update": can_update,
            "can_delete": can_delete}


@router.get("/document/{id}", status_code=status.HTTP_200_OK)
async def get_document_permission(id: int,
                                  username: str = Depends(check_current_credentials)):
    """
    Gets document permission for given id.
    """

    prm = await DocumentPermission.get(id=id).prefetch_related("document").prefetch_related("member")
    return await document_pydantic.from_tortoise_orm(prm)


@router.get("/category/{id}", status_code=status.HTTP_200_OK)
async def get_category_permission(id: int,
                                  username: str = Depends(check_current_credentials)):
    """
    Gets category permission for given id.
    """

    prm = await CategoryPermission.get(id=id).prefetch_related("category").prefetch_related("member")
    return await category_pydantic.from_tortoise_orm(prm)


@router.delete("/document/{id}", status_code=status.HTTP_200_OK)
async def delete_document_permission(id: int,
                                     username: str = Depends(check_admin_credentials)):
    """
    Deletes document permission for given id.
    """

    prm = await DocumentPermission.get(id=id)
    await DocumentPermission.delete(prm)
    return await document_pydantic.from_tortoise_orm(prm)


@router.delete("/category/{id}", status_code=status.HTTP_200_OK)
async def delete_category_permission(id: int,
                                     username: str = Depends(check_admin_credentials)):
    """
    Deletes category permission for given id.
    """

    prm = await CategoryPermission.get(id=id)
    await CategoryPermission.delete(prm)
    return await category_pydantic.from_tortoise_orm(prm)


@router.post("/create", status_code=status.HTTP_201_CREATED)
async def create_permission(prm: PermissionDto,
                            username: str = Depends(check_admin_credentials)):
    """
    Creates permission.
    """

    category = None
    document = None
    if prm.category_id not in (None, 0) and prm.document_id in (None, 0):
        category = await Category.get(id=prm.category_id)
    if prm.document_id not in (None, 0) and prm.category_id in (None, 0):
        document = await Document.get(id=prm.document_id)

    if category is None and document is None:
        message = "One of (and only one of) category or document fields should be passed."
        logger.error(message)

        raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                            detail=message)

    member = await Member.get(id=prm.member_id)
    if category is not None:
        already_added = await CategoryPermission.get_or_none(category=category,
                                                             member=member)
        if already_added:
            message = f"Permission already defined for category {category.name} and member {member.username}."
            logger.error(message)

            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail=message)

        added = await CategoryPermission.create(category=category,
                                                member=member,
                                                can_create=prm.can_create,
                                                can_read=prm.can_read,
                                                can_delete=prm.can_delete,
                                                can_update=prm.can_update)
        return await category_pydantic.from_tortoise_orm(added)

    if document is not None:
        already_added = await DocumentPermission.get_or_none(document=document,
                                                             member=member)
        if already_added:
            message = f"Permission already defined for document {document.name} and member {member.username}."
            logger.error(message)

            raise HTTPException(status_code=status.HTTP_400_BAD_REQUEST,
                                detail=message)

        added = await DocumentPermission.create(document=document,
                                                member=member,
                                                can_create=prm.can_create,
                                                can_read=prm.can_read,
                                                can_delete=prm.can_delete,
                                                can_update=prm.can_update)
        return await document_pydantic.from_tortoise_orm(added)
