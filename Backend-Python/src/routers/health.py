from fastapi import APIRouter, Depends
from fastapi_health import health
from tortoise import Tortoise
from src.config import tortoise_config


async def get_session():
    await Tortoise.init(
        db_url=tortoise_config.db_url,
        modules=tortoise_config.modules,
    )
    return True


def is_database_online(session: bool = Depends(get_session)):
    return { 'database': session }


router = APIRouter()
router.add_api_route("", health([is_database_online]))
