from fastapi import APIRouter, Depends, status

from tortoise.contrib.pydantic import pydantic_model_creator

from src.auth.basic import check_admin_credentials
from src.models.dto.member import MemberDto
from src.models.entity.member import Member

router = APIRouter()
member_pydantic = pydantic_model_creator(Member)


@router.get("/{id}", status_code=status.HTTP_200_OK)
async def get_member(id: int):
    """
    Gets member for given id.
    """

    member = await Member.get(id=id)
    return await member_pydantic.from_tortoise_orm(member)


@router.post("/create", status_code=status.HTTP_201_CREATED)
async def create_member(member: MemberDto,
                        username: str = Depends(check_admin_credentials)):
    """
    Creates member, only via admin.
    """

    added = await Member.create(username=member.username)
    return await member_pydantic.from_tortoise_orm(added)

