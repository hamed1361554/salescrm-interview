from fastapi import APIRouter, Depends, status
from tortoise.contrib.pydantic import pydantic_model_creator

from src.auth.basic import check_current_credentials
from src.models.dto.document import DocumentDto
from src.models.entity.document import Document, Category, Author
from src.models.entity.member import Member
from src.models.entity.permission import CategoryPermission, DocumentPermission

router = APIRouter()
author_pydantic = pydantic_model_creator(Author)
category_pydantic = pydantic_model_creator(Category)
document_pydantic = pydantic_model_creator(Document)


@router.get("/", status_code=status.HTTP_200_OK)
async def get_documents(username: str = Depends(check_current_credentials)):
    """
    Gets all documents for given id.
    """

    member = await Member.get(username=username)
    docs = await Document.all().select_related("author").select_related("category")

    results = []
    for doc in docs:
        model = await document_pydantic.from_tortoise_orm(doc)
        result = model.dict()

        author = await author_pydantic.from_tortoise_orm(doc.author)
        result["author"] = author.dict()

        category = await category_pydantic.from_tortoise_orm(doc.category)
        result["category"] = category.dict()

        can_create = False
        can_read = False
        can_update = False
        can_delete = False

        cat_permission = await CategoryPermission.get_or_none(category=doc.category,
                                                              member=member)
        if cat_permission is not None:
            can_create = cat_permission.can_create
            can_read = cat_permission.can_read
            can_update = cat_permission.can_create
            can_delete = cat_permission.can_create

        doc_permission = await DocumentPermission.get_or_none(document=doc,
                                                              member=member)
        if doc_permission is not None:
            if doc_permission.can_create:
                can_create = True

            if doc_permission.can_read:
                can_read = True

            if doc_permission.can_update:
                can_update = True

            if doc_permission.can_delete:
                can_delete = True

        result["can_create"] = can_create
        result["can_read"] = can_read
        result["can_update"] = can_update
        result["can_delete"] = can_delete
        results.append(result)

    return results


@router.get("/{id}", status_code=status.HTTP_200_OK)
async def get_document(id: int,
                       username: str = Depends(check_current_credentials)):
    """
    Gets document for given id.
    """

    doc = await Document.get(id=id).prefetch_related("author").prefetch_related("category")
    return await document_pydantic.from_tortoise_orm(doc)


@router.delete("/{id}", status_code=status.HTTP_200_OK)
async def delete_document(id: int,
                          username: str = Depends(check_current_credentials)):
    """
    Gets document for given id.
    """

    doc = await Document.get(id=id)
    await Document.delete(doc)
    return await document_pydantic.from_tortoise_orm(doc)


@router.post("/create", status_code=status.HTTP_201_CREATED)
async def create_document(doc: DocumentDto,
                          username: str = Depends(check_current_credentials)):
    """
    Creates document.
    """

    category = await Category.get(id=doc.category_id)
    author = await Author.get(id=doc.author_id)

    added = await Document.create(name=doc.name,
                                  subject=doc.subject,
                                  content=doc.content,
                                  category=category,
                                  author=author)
    return await document_pydantic.from_tortoise_orm(added)
