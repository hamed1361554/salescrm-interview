from fastapi import APIRouter, Depends, status
from tortoise.contrib.pydantic import pydantic_model_creator

from src.auth.basic import check_current_credentials
from src.models.dto.document import CategoryDto
from src.models.entity.document import Category

router = APIRouter()
category_pydantic = pydantic_model_creator(Category)


@router.get("/{id}", status_code=status.HTTP_200_OK)
async def get_category(id: int,
                       username: str = Depends(check_current_credentials)):
    """
    Gets category for given id.
    """

    cat = await Category.get(id=id)
    return await category_pydantic.from_tortoise_orm(cat)


@router.post("/create", status_code=status.HTTP_201_CREATED)
async def create_category(cat: CategoryDto,
                          username: str = Depends(check_current_credentials)):
    """
    Creates category.
    """

    added = await Category.create(name=cat.name)
    return await category_pydantic.from_tortoise_orm(added)
