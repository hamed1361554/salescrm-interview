import secrets

from fastapi import Depends, FastAPI, HTTPException, status
from fastapi.security import HTTPBasic, HTTPBasicCredentials

from src.models.entity.member import Member

app = FastAPI()

security = HTTPBasic()


async def check_admin_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    current_username_bytes = credentials.username.encode("utf8")
    correct_username_bytes = b"admin"
    is_correct_username = secrets.compare_digest(current_username_bytes,
                                                 correct_username_bytes)

    current_password_bytes = credentials.password.encode("utf8")
    correct_password_bytes = b""
    is_correct_password = secrets.compare_digest(current_password_bytes,
                                                 correct_password_bytes)

    if not (is_correct_username and is_correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect name or password",
            headers={"WWW-Authenticate": "Basic"},
        )


async def check_current_credentials(credentials: HTTPBasicCredentials = Depends(security)):
    current_username_bytes = credentials.username.encode("utf8")
    member = await Member.get_or_none(username=credentials.username)
    if member is None:
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect name or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    correct_username_bytes = member.username.encode("utf8")
    is_correct_username = secrets.compare_digest(current_username_bytes,
                                                 correct_username_bytes)

    current_password_bytes = credentials.password.encode("utf8")
    correct_password_bytes = b""
    is_correct_password = secrets.compare_digest(current_password_bytes,
                                                 correct_password_bytes)

    if not (is_correct_username and is_correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect name or password",
            headers={"WWW-Authenticate": "Basic"},
        )

    return credentials.username
