from pydantic import BaseModel


class CategoryDto(BaseModel):
    name: str


class AuthorDto(BaseModel):
    name: str


class DocumentDto(BaseModel):
    name: str
    subject: str
    content: str
    category_id: int
    author_id: int
