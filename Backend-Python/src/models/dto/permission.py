from typing import Optional

from pydantic import BaseModel


class PermissionDto(BaseModel):
    category_id: Optional[int] = None
    document_id: Optional[int] = None
    member_id: int
    can_create: bool
    can_read: bool
    can_update: bool
    can_delete: bool


class PermissionQueryDto(BaseModel):
    document_id: int
    member_id: int
