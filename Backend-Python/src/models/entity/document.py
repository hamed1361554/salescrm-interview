from tortoise import Model, fields


class Category(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)


class Author(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)


class Document(Model):
    id = fields.IntField(pk=True)
    name = fields.CharField(max_length=255)
    subject = fields.CharField(max_length=255)
    content = fields.CharField(max_length=255)
    category = fields.ForeignKeyField('models.Category', related_name='category')
    author = fields.ForeignKeyField('models.Author', related_name='author')
