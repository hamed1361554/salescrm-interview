from tortoise import Model, fields


class CategoryPermission(Model):
    id = fields.IntField(pk=True)
    category = fields.ForeignKeyField('models.Category', related_name='permissioncategory')
    member = fields.ForeignKeyField('models.Member', related_name='categorymember')
    can_create = fields.BooleanField(default=False)
    can_read = fields.BooleanField(default=False)
    can_update = fields.BooleanField(default=False)
    can_delete = fields.BooleanField(default=False)


class DocumentPermission(Model):
    id = fields.IntField(pk=True)
    document = fields.ForeignKeyField('models.Document', related_name='permissiondocument')
    member = fields.ForeignKeyField('models.Member', related_name='documentmember')
    can_create = fields.BooleanField(default=False)
    can_read = fields.BooleanField(default=False)
    can_update = fields.BooleanField(default=False)
    can_delete = fields.BooleanField(default=False)
