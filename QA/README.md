## sample questions

1-How many years of work experience do you have in the QA? 

2-Name the tools you used in the test discussion and give a brief description of each. 

3-Describe the Functional test

4-Describe the Smoke test

5-Describe the Sanity test

6-Describe the Regression test

7-Suppose Google adds a scheduled email service to its Gmail service. Describe the test process you are going through for this new feature. 

8-Suppose you found a bug in the previous question and you want to report it and create a task for it. Create a sample of your report and your task and attach it here 


https://forms.gle/bQf2vuE5A5RwHvmEA
